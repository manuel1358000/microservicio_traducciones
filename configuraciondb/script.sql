CREATE TABLE usuarios (
    cod_usuario serial PRIMARY KEY,
    nombre varchar(255),
    usuario varchar(255),
    password varchar(255)
);
insert into usuarios(nombre,usuario,password)values('Manuel Fuentes','manuel','manuel');
insert into usuarios(nombre,usuario,password)values('Luis Ramirez','luis','luis');
insert into usuarios(nombre,usuario,password)values('Jorge Santos','jorge','jorge');
insert into usuarios(nombre,usuario,password)values('Antonio Fuentes','antonio','antonio');
insert into usuarios(nombre,usuario,password)values('Roberto Palma','roberto','roberto');
insert into usuarios(nombre,usuario,password)values('Javier Lopez','javier','javier');
insert into usuarios(nombre,usuario,password)values('Miguel Rodriguez','miguel','miguel');